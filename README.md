# Atm Locator

This is my implementation of a basic Atm Locator for the Netherlands' ING.

It's a Java Web Application based on the Spring Framework version 4.

# Installation

The architecture was based on a Spring Boot app so a standalone java application is available to download:

[Atm Locator - Spring Boot 1.1.3](https://drive.google.com/file/d/0B_IkA-OZ3V_1Znk2YlN6UG1RRU0/view?usp=sharing)

To run this application in a terminal window run:

    java -jar backbase-ing-atm-locator-0.0.1.jar

It's also possible to deploy the app into an Application Server but I took the opportunity to ignore the Tomcat 7 requirement to implement a lambda function to filter the results that came from the ING Api. 

That decision made Java 8 a requirement. 

[Atm Locator - War file](https://drive.google.com/file/d/0B_IkA-OZ3V_1NnhFalhrV3FRdEE/view?usp=sharing)

Althought the backend runs smoothly, the front end was developed with the assumption that it was running at the root folder of a domain (e.g. http://localhost:8080). 
Because of that this version have errors that I was not able to fix in time.

# Architecture

## Front end

The Front end is a [Polymer](www.polymer-project.org) app based on its Starter project for simplicity. The following components were used:

    Iron Elements
    Paper Elements
    Google Map Component
    
The Front end source code is stored on this project [Link]. In this backend project the resulting [gulp](gulpjs.com) build is stored in the src/main/resources/static folder.

With that we would be able to pack everything together a following version could take advantage of maven or a continuous integration system to automate that.

That's probably not the best Polymer implementation but I just learned the framework in order to produce this version.

## Back end

The Back end is a Spring Boot Web Application configured with Java that makes use of some Spring projects to perform its tasks

### Spring MVC

Spring MVC was used to easy build the communication between the front end and the ING service. 

It provides the ability to route urls to methods as long to automatically get information direct from the urls as path variables and along with Jackson parse objects to json as responses.

It also provides the RestTemplate interface that easy the consumption of third party REST services.

### Apache Camel

Apache Camel is an Enterprise Integration Framework that allows us to connect almost anything to anything. In the scenario of this project it was used to build the connectivity to the ING Atm Locator Service.

### Spring Security

Spring Security was used to secure our application. A simple configuration was defined that provides a simple page that accept "user" "password" as credentials. 

The whole app is secured so you can't use it without providing a password.

This application doesn't connect to any datasource so an in memory data source was provided to hold all the "users". 

## Usage

After the application is running you can access the application by accessing the root folder of the server at port 8080.

It will redirect you to the login page where you'll use "user" "password" as credentials. 

With the right credentials you'll be able to use the application. As soon as you start filling up the of the desired city, the front end application will fire a request to the backend in order to search for Atms.

The ATM count will stay at 0 while a valid city name is not entered. Once you choose a city with ING Atms, the app will setup markers on the map showing the atms. Bellow the map more information around all the atms founded are listed.

## Known Issues

This application is not production ready its missing some important features:

* There's no error handling implemented, when something goes wrong the end user is not notified in a proper way;
* There's no internationalization implemented, everything is presented in a single language
* The application is not monitoring ready. It doesn't test is self on the fly and don't provide any metrics that could be monitored.
* The developer just learned Apache Camel and Polymer to build this so probably the best practices were not followed.
* No Unit testing implemented Given the focus on the new frameworks, just the basic test that checks if the Context is loadable was developed.

