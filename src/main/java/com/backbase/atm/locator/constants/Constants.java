package com.backbase.atm.locator.constants;


/**
 * This class holds applications constants.
 * @author guilhermeantunes
 *
 */
public class Constants {

	public static final String SERVICE_URL = "https://www.ing.nl/api/locator/atms/";

}
