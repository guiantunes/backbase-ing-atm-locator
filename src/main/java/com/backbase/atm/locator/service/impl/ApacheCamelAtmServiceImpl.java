package com.backbase.atm.locator.service.impl;

import java.io.IOException;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.backbase.atm.locator.model.Atm;
import com.backbase.atm.locator.service.AtmService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * Apache Camel implementation of the ATM service
 *
 * @author guilhermeantunes
 *
 */
public class ApacheCamelAtmServiceImpl extends AtmServiceBaseImpl implements AtmService {	
	
	private static final Logger logger = LoggerFactory.getLogger(ApacheCamelAtmServiceImpl.class);
	
	@Autowired
	private ObjectMapper mapper;

	@Produce(uri = "direct:start")
    private ProducerTemplate template;

	@Override
	public List<Atm> getAtmListFromService() {
		
		Exchange exchange = template.send("direct://start", new Processor() {
		    public void process(Exchange exchange) throws Exception {
		        exchange.setPattern(ExchangePattern.InOut);
		        Message inMessage = exchange.getIn();
		        // setup the accept content type
		        inMessage.setHeader(Exchange.ACCEPT_CONTENT_TYPE, "text/plain");
		    }
		});
		      
		// get the response message 
		String response = exchange.getOut().getBody(String.class);
		
		response = response.substring(response.indexOf('['));
		
		List<Atm> atms = null;
		
		if (mapper != null) {
			try {
				atms = mapper.readValue(response, TypeFactory.defaultInstance().constructCollectionType(List.class, Atm.class));
			} catch (IOException e) {
				logger.error("Exception", e);
			}
		} else {
			logger.error("Mapper is null, dependency injection failed!");
		}
		
		return atms;
	}

}
