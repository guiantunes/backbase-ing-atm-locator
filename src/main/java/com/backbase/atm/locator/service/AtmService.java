package com.backbase.atm.locator.service;

import java.util.List;

import com.backbase.atm.locator.model.Atm;

/**
 * Interface that defines the service that brings Atm information for a given city
 * @author guilhermeantunes
 *
 */
public interface AtmService {
	
	/**
	 * Method that return a list of Atms by a given city name
	 * @param cityName
	 * @return
	 */
	public List<Atm> getAtmByCity (String cityName);

}
