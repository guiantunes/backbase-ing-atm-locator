package com.backbase.atm.locator.service.impl;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import com.backbase.atm.locator.constants.Constants;
import com.backbase.atm.locator.model.Atm;
import com.backbase.atm.locator.service.AtmService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

/**
 * Spring RestTemplate implementation of the AtmService
 * @author guilhermeantunes
 *
 */
public class SpringRestTemplateAtmServiceImpl extends AtmServiceBaseImpl implements AtmService {
	
	private static final Logger logger = LoggerFactory.getLogger(SpringRestTemplateAtmServiceImpl.class);
	
	@Autowired
	private ObjectMapper mapper;

	@Override
	public List<Atm> getAtmListFromService() {
		RestTemplate template = new RestTemplate();
		
		String serviceResponse = template.getForObject(Constants.SERVICE_URL, String.class);
		
		logger.info("Service Response: {}", serviceResponse);
		serviceResponse = serviceResponse.substring(serviceResponse.indexOf('['));
		logger.info("Service Response: {}", serviceResponse);
		
		List<Atm> atms = null;
		
		if (mapper != null) {
			try {
				atms = mapper.readValue(serviceResponse, TypeFactory.defaultInstance().constructCollectionType(List.class, Atm.class));
			} catch (IOException e) {
				logger.error("Exception", e);
			}
		} else {
			logger.error("Mapper is null, dependency injection failed!");
		}
		
		return atms;
	}

}
