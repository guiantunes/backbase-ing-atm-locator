package com.backbase.atm.locator.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.backbase.atm.locator.model.Atm;
import com.backbase.atm.locator.service.AtmService;

/**
 * Base implementation of the Atm Service, handles common tasks.
 * 
 * @author guilhermeantunes
 *
 */
public abstract class AtmServiceBaseImpl implements AtmService{
	

	/**
	 * This method calls internal method to interact with the web service than filter the result by the given city name
	 */
	@Override
	public final List<Atm> getAtmByCity(String cityName) {
		List<Atm> atmList = getAtmListFromService();
		
		List<Atm> result = atmList.parallelStream()
								  .filter(v -> v.getAddress().getCity().toUpperCase().equals(cityName.toUpperCase()))
								  .collect(Collectors.toList());		
		
		return result;
		
	}
	
	/**
	 * Call ING to get atm list
	 * @return
	 */
	public abstract List<Atm> getAtmListFromService ();

}
