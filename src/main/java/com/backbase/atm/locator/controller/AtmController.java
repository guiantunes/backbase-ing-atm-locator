package com.backbase.atm.locator.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backbase.atm.locator.model.Atm;
import com.backbase.atm.locator.service.AtmService;

/**
 * Spring MVC Rest interface, manages the communication with the front-end
 * @author guilhermeantunes
 *
 */
@RestController
public class AtmController {
	
	private static final Logger logger = LoggerFactory.getLogger(AtmController.class);
	
	@Autowired
	private AtmService atmService;

	/**
	 * Handles requests from the front end, get the city name to query from the url and uses the AtmService defined to get information
	 * It's marked for open cross origin so it could handle requests from other websites.
	 * 
	 * @param city
	 * @return
	 */
	@CrossOrigin (origins = "*")
	@RequestMapping("/api/atm/{city}")
	public List<Atm> getAtmByCity (@PathVariable String city) {
		
		logger.info("City {}", city);
		
		if (StringUtils.isEmpty(city)){
			return new ArrayList<Atm>();
		}
		
		List<Atm> response = atmService.getAtmByCity(city);
		
		logger.debug("Response: {}", response);
		
		return response;
		
	}

	/**
	 * Handles requests when no city is present in the url, it returns an empty list when called.
	 * @return
	 */
	@CrossOrigin (origins = "*")
	@RequestMapping("/api/atm")
	public List<Atm> getAtmByCityEmpty () {
		
			return new ArrayList<Atm>();
		
	}
	
	
}
