package com.backbase.atm.locator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

import com.backbase.atm.locator.service.AtmService;
import com.backbase.atm.locator.service.impl.ApacheCamelAtmServiceImpl;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Spring boot application initializer
 * 
 * @author guilhermeantunes
 *
 */
@SpringBootApplication
public class BackbaseIngAtmLocatorApplication extends SpringBootServletInitializer {

	/**
	 * Starts the applications
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(BackbaseIngAtmLocatorApplication.class, args);
	}
	
	/**
	 * Jackson objectmapper that handles translations between java objects and json objects
	 * @return
	 */
	@Bean
	public ObjectMapper mapper () {
		ObjectMapper mapper = new ObjectMapper();
		
		return mapper;
	}
	
	/**
	 * Atm Service declarations. this allows us to switch between Apache Camel and Spring RestTemplate implementations
	 * @return
	 */
	@Bean
	public AtmService atmService() {
//		return new SpringRestTemplateAtmServiceImpl();
		return new ApacheCamelAtmServiceImpl();
	}
}
