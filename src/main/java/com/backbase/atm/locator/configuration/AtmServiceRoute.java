package com.backbase.atm.locator.configuration;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

import com.backbase.atm.locator.constants.Constants;

/**
 * Configure Camel Routes
 * @author guilhermeantunes
 *
 */
@Component
public class AtmServiceRoute extends RouteBuilder{

	@Override
	public void configure() throws Exception {		
		
		from ("direct:start").to(Constants.SERVICE_URL);
		
	}

}
