package com.backbase.atm.locator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.backbase.atm.locator.service.AtmService;
import com.backbase.atm.locator.service.impl.MockAtmService;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = BackbaseIngAtmLocatorApplication.class)
@WebAppConfiguration
public class BackbaseIngAtmLocatorApplicationTests {

	@Test
	public void contextLoads() {
	}
	

	@Bean
	public AtmService atmService() {
		return new MockAtmService();
	}

}
