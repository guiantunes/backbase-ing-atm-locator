package com.backbase.atm.locator.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.backbase.atm.locator.model.Address;
import com.backbase.atm.locator.model.Atm;

public class MockAtmService extends AtmServiceBaseImpl {

	@Override
	public List<Atm> getAtmListFromService() {
		List<Atm> atms = new ArrayList<Atm>();

		Address amsterdam = new Address();
		amsterdam.setCity("Amsterdam");
		Address eindhoven = new Address();
		eindhoven.setCity("Eindhoven");
		
		Atm atm = new Atm();
		atm.setAddress(amsterdam);		
		atms.add(atm);
		
		atm = new Atm();
		atm.setAddress(amsterdam);
		atms.add(atm);
		
		atm = new Atm();
		atm.setAddress(eindhoven);
		atms.add(atm);
		
		return atms;
	}

}
